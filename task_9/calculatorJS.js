let resolveForOperator = false; // Чтоб не было несколько операторов подряд
let result = '';
let endOfOperation = false;
let firstDigitInValue = true; // Когда true, ноль не станет первой цифрой в числе
let closedHistory = true;
let historyStr = historyFunc();

function historyFunc() {
    let history = '';
    let firstOperation = true;

    return function(str = false) {
        if (firstOperation) {
            history += str;
            firstOperation = false;
            return history;
        }
        if (str) {
            history += '\n' + str;
        }

        return history;
    }
}

document.addEventListener('mouseover', mouseoverFunc);
document.addEventListener('mouseout', mouseoutFunc);
document.addEventListener('mousedown', mousedownFunc); // Обеспечивает изменение цвета клавиши при нажатии
document.addEventListener("mouseup", mouseupFunc);
document.addEventListener('click', clickFunc); /* Реализует добавление чисел и операторов,
 а также их расчет в output при клике. Запускает функцию historyFunc. */

function mouseoverFunc() {
    if (event.target.id.slice(0, -1) === 'digit' ||
        event.target.id.slice(0, -1) === 'operator' ||
        event.target.id === 'historyButton') {
        document.getElementById(String(event.target.id)).style.background = '#cbd0d0';
        document.getElementById(String(event.target.id)).style.borderColor = '#cbd0d0';
    }
}

function mouseoutFunc() {
    if (event.target.id.slice(0, -1) === 'digit' ||
        event.target.id.slice(0, -1) === 'operator' ||
        event.target.id === 'historyButton') {
        document.getElementById(String(event.target.id)).style.background = '#dde2e2';
        document.getElementById(String(event.target.id)).style.borderColor = '#dde2e2';
    }
}

function mousedownFunc() {
    if (event.target.id.slice(0, -1) === 'digit' ||
        event.target.id.slice(0, -1) === 'operator' ||
        event.target.id === 'historyButton') {
        document.getElementById(String(event.target.id)).style.background = '#acb0b0';
        document.getElementById(String(event.target.id)).style.borderColor = '#5e6262';
    }
}


function mouseupFunc() {
    if (event.target.id.slice(0, -1) === 'digit' ||
        event.target.id.slice(0, -1) === 'operator' ||
        event.target.id === 'historyButton') {
        document.getElementById(String(event.target.id)).style.background = '#cbd0d0';
        document.getElementById(String(event.target.id)).style.borderColor = '#cbd0d0';
    }
}

function clickFunc() {
    let target = document.getElementById(String(event.target.id));

    if (target.id === 'historyButton') {
        if (closedHistory) {
            document.getElementById('history').style.visibility = 'visible';
            closedHistory = false
        } else {
            document.getElementById('history').style.visibility = 'hidden';
            closedHistory = true;
        }
    }
    if (firstDigitInValue && target.id === 'digit0') {
        return;
    }
    if (error.innerHTML) {
        if (target.innerHTML === 'C') {
            error.innerHTML = null;
        }

        return;
    }
    if (endOfOperation === true && Math.floor(eval(result)) === 0) {
        firstDigitInValue = true;
        resolveForOperator = false;
        endOfOperation = false;
        resolve.innerHTML = null;
        output.innerHTML = null;

        return result = '';
    }
    if (endOfOperation === true && target.id.slice(0, -1) === 'digit') {
        endOfOperation = false;
        output.innerHTML = null;
        resolve.innerHTML = null;
        result = '';
    }
    if (endOfOperation === true) {
        endOfOperation = false;
        resolve.innerHTML = null;
        output.innerHTML = Math.floor(eval(result));
        result = Math.floor(eval(result));
    }
    if (target.innerHTML === 'C') {
        firstDigitInValue = true;
        endOfOperation = false;
        resolveForOperator = false;
        resolve.innerHTML = null;
        output.innerHTML = null;

        return result = '';
    }
    if (target.innerHTML === '=' && resolveForOperator) {

        if (`${result} = ${Math.floor(eval(result))}`.length > 100) {
            firstDigitInValue = true;
            endOfOperation = false;
            resolveForOperator = false;
            resolve.innerHTML = null;
            output.innerHTML = null;
            error.innerHTML = 'Out of range! ' +
                '\nMaximum quantity of symbols is 100.' +
                '\nPress <span style="color: red">C</span> to continue.';

            return result = '';
        }
        if (`${result} = ${Math.floor(eval(result))}`.length > 45) {
            output.style.fontSize = '20px';
            resolve.style.fontSize = '20px';
        }

        historyStr(`${result} = ${Math.floor(eval(result))}`);
        document.getElementById('history').innerHTML = historyStr();

        firstDigitInValue = true;
        endOfOperation = true;

        return resolve.innerHTML = `<span style="color: #676a6a">=</span> ${Math.floor(eval(result))}`;
    }
    if (target.id.slice(0, -1) === 'digit' && endOfOperation === false) {
        if (result.length > 100) {
            endOfOperation = false;
            resolveForOperator = false;
            resolve.innerHTML = null;
            output.innerHTML = null;
            error.innerHTML = 'Out of range! ' +
                '\nMaximum quantity of symbols is 100.' +
                '\nPress <span style="color: red">C</span> to continue.';

            return result = '';
        }
        if (result.length > 30) {
            output.style.fontSize = '20px';
        } else {
            output.style.fontSize = '40px';
            resolve.style.fontSize = '40px';
        }
        firstDigitInValue = false;
        output.innerHTML += target.innerHTML;
        resolveForOperator = true;
        result += target.innerHTML;
    }
    if (target.id.slice(0, -1) === 'operator' &&
        resolveForOperator === true && target.innerHTML !== '=') {
        firstDigitInValue = true;
        resolveForOperator = false;
        output.innerHTML += '<span style="color: #676a6a"> ' + target.innerHTML + ' </span>';
        result += target.innerHTML;
    }
}