const fakeUrlArray = ['url 1', 'url 2', 'url 3', 'url 4', 'url 5', 'url 6'];

function request(url) {
    return new Promise((res, rej) => {
        const delayTime = Math.floor(Math.random() * 10000) + 1;

        setTimeout(() => res(url), delayTime);
    });
}

function promiceToUrlArray(urlArray) {

    return new Promise(resolve => {
        let index = 0;
        let doneArray = [];

        syncResolving();

        function syncResolving() {

            request(urlArray[index])
                .then(doneUrl => {

                    console.log(doneUrl, 'час виконання ---->', Date().slice(16, -20));
                    doneArray.push(doneUrl);

                    if (++index < urlArray.length) {
                        syncResolving();
                    }else{
                        resolve(doneArray);
                    }
                })
            }
    })
}

promiceToUrlArray(fakeUrlArray).then(doneArray => console.log(doneArray));

// url 1 час виконання ----> 20:21:06
// url 2 час виконання ----> 20:21:11
// url 3 час виконання ----> 20:21:16
// url 4 час виконання ----> 20:21:17
// url 5 час виконання ----> 20:21:23
// url 6 час виконання ----> 20:21:24
// [ 'url 1', 'url 2', 'url 3', 'url 4', 'url 5', 'url 6' ]