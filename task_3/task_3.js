function multiplier(num1) {             //task 1
     return function(num2) {
        return num1 * num2;
    };
}

function processData(input) {
    const waterWeight = multiplier(1000);
    const mercuryWeight = multiplier(1355);
    const oilWeight = multiplier(900);

    console.log("Weight of " + input + " metric cube of water = " + waterWeight(input) + " kg");
    console.log("Weight of " + input + " metric cube of mercury = " + mercuryWeight(input) + " kg");
    console.log("Weight of " + input + " metric cube of oil = " + oilWeight(input) + " kg");
}

// processData(2);

let randomArrayElement = (array) => {           // Варіант з індексом вийшов більш складним, тому цей варіант здається кращим.
    return array[Math.floor(Math.random() * array.length)];
};

let makeRandomFromArray = (array) => {             //task 2
    return function() {
        return randomArrayElement(array);
    };
};

const getRandomNumber = makeRandomFromArray([1, 2, 100, 34, 45, 556, 33]);

// console.log(getRandomNumber()); // 556
// console.log(getRandomNumber()); // 100
// console.log(getRandomNumber()); // 2

let makeRandomFn = (...arr) => {             //task 3

    if (typeof arr[0] === 'object') {
        let arrayFromArr = [];

        for (let number of arr[0]){
            arrayFromArr.push(number);
        }

        return randomArrayElement(arrayFromArr);
    }

    return randomArrayElement(arr);
};

// console.log(makeRandomFn(1, 2, 100, 34, 45, 556, 33));
// console.log(makeRandomFn([1, 2, 100, 34, 45, 556, 33]));