const users = [
    {
        balance: '3946.45',
        picture: 'http://placehold.it/32x32',
        age: 23,
        name: 'Bird Ramsey',
        gender: 'male',
        company: 'NIMON',
        email: 'birdramsey@nimon.com',
    },
    {
        balance: '2499.49',
        picture: 'http://placehold.it/32x32',
        age: 31,
        gender: 'female',
        company: 'LUXURIA',
        email: 'lillianburgess@luxuria.com',
    },
    {
        balance: '2820.18',
        picture: 'http://placehold.it/32x32',
        age: 34,
        name: 'Kristie Cole',
        gender: 'female',
        company: 'QUADEEBO',
        email: 'kristiecole@quadeebo.com',
    },
    {
        balance: '1277.32',
        picture: 'http://placehold.it/32x32',
        age: 30,
        gender: 'female',
        company: 'GRONK',
        email: 'leonorcross@gronk.com',
    },
    {
        balance: '1972.47',
        picture: 'http://placehold.it/32x32',
        age: 28,
        gender: 'male',
        company: 'ULTRIMAX',
        email: 'marshmccall@ultrimax.com',
    },
];

function getNameFromCurrentContex(object) {
    return object.name;
}

function rejectNoNameUsers(array) {
    let usersWithNames = [];

    for (let object of array) {
        if (getNameFromCurrentContex(object)) {
            usersWithNames.push(object);
        }
    }

    return usersWithNames;
}

// const usersWithNames = rejectNoNameUsers(users);
// console.log(usersWithNames);