// Task 1.
// Создайте функцию, которая на вход получаете строку "a.b.c.d"
const string = "a.b.c.d";

function makeManyObjects(str) {
    return str.split('.')
        .reverse()
        .reduce(function (previousValue, current) {
            return {[current]: previousValue};
        }, null);
}

// console.log(makeManyObjects(string));

// Task 2
// Написать функцию, которая находит пересечение двух массивов

function findEquals(array1, array2) {
    return array1.concat(array2)
        .filter(function(number, i, arr) {
        return array1.indexOf(number) !== -1 && array2.indexOf(number) !== -1 &&
            arr.indexOf(number, i + 1) === -1;
    });
}

// console.log(findEquals([2, 6, 3, 7, 5], [1, 2, 3, 7, 9, 9, 9])); //[ 2, 3, 7 ]

// Task 3
// Напишите функцию для объединения двух массивов и удаления всех дублирующих элементов.

const firstArray = [1, 2, 3];
const secondArray = [2, 30, 1];

function mergeArray(array1, array2) {
    return array1.concat(array2)
        .sort()
        .filter(function(number, i, arr) {
        return number !== arr[i + 1];
    });
}

const mergedArray = mergeArray(firstArray, secondArray); // [1, 2, 30, 3]

// console.log(mergedArray);

// Task 4
// Напишите функцию mapObject, которая преобразовывает значения
// всех свойств объекта (аналог map из массива, только для объекта, возвращает новый объект)

const double = x => x * 2;

function mapObject(functionForObj, obj) {

    for (let key in obj) {
        obj[key] = functionForObj(obj[key]);
    }

    return obj;
}

// console.log(mapObject(double, {x: 1, y: 2, z: 3})); //=> {x: 2, y: 4, z: 6}