class Person {

    constructor(firstname, lastname, age) {
    try {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;

        let latinLetters = new RegExp('^[a-zA-Z]+$');

        if (!latinLetters.test(firstname) || !latinLetters.test(lastname)) {
            throw new Error("Имена должны содержать только латинские буквы!");
        } else if (firstname.length < 3 || lastname.length < 3 ||
            firstname.length > 20 || lastname.length > 20) {
            throw new Error("firstname и lastname всегда должны быть строки от 3 до 20 символов!");
        } else if (age < 0 || age > 150) {
            throw new Error("age всегда должно быть числом в диапазоне (0, 150) включительно!");
        }

    } catch (err) {
        console.log(err.message);
    }
    }

    getFullname() {
    return `${this.firstname} ${this.lastname}`;
    }

    setFullname(firstAndLastName) {
    const namesArray = firstAndLastName.split(' ');
    this.firstname = namesArray[0];
    this.lastname = namesArray[1];
    }

    introduce() {
    return `Hello! My name is ${this.getFullname()} and I am ${this.age} years-old.`;
    }
}

class Worker extends Person {

    constructor(firstname, lastname, age, experience, salary) {
    try {
        super(firstname, lastname, age);
        this.experience = experience;
        this.salary = salary;
        this.RemunerationRate = this.getRemunerationRate();

        if (experience < 1 || experience > 50) {
            throw new Error("experience всегда должно быть числом в диапазоне (1, 50) включительно!");
        } else if (salary < 1000 || salary > 10000) {
            throw new Error("salary всегда должно быть числом в диапазоне (1000, 10000) включительно");
        }

    } catch (err) {
        console.log(err.message);
    }
    }

    getRemunerationRate() {
        let P;

        if (this.experience >= 1 || this.experience <= 5) {
            P = 5;
        } else if (this.experience >= 6 || this.experience <= 10) {
            P = 10;
        } else if (this.experience >= 11 || this.experience <= 20) {
            P = 20;
        } else if (this.experience > 20) {
            P = 50;
        }

        return this.experience * P / 100;
    }
}

let Andy = new Person('Andy', 'Johnson', 0);

let Andrew = new Worker('Andrew', 'Allen', 24, 10, 5000);

// Andy.setFullname('Mary Williams');
// console.log(Andy);
// console.log(Andy.getFullname());
// console.log(Andy.introduce());
//
// Andrew.setFullname('Mary Williams');
// console.log(Andrew);
// console.log(Andrew.getFullname());
// console.log(Andrew.introduce());
// console.log(Andrew.getRemunerationRate());