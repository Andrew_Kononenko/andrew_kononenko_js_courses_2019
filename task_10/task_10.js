//---------------------------------exercise_1------------------------------------------------
function plusOne(x) {
    return x + 1;
}

function negative(x) {
    return -x;
}

function compose() {
    let functions = arguments;

    return function() {
        let composed;

        for (let funcNumb = functions.length - 1; funcNumb > -1; funcNumb--) {

            if (funcNumb === functions.length - 1) {
                composed = functions[funcNumb].apply(null, arguments);
            } else {
                composed = functions[funcNumb](composed);
            }
        }

        return composed;
    }
}

// let f = compose(plusOne, negative, Math.pow);
//
// console.log(f(3, 4));

//---------------------------------exercise_2------------------------------------------------

function printLetters(a, b, c, d) {
    console.log(`${a} ${b} ${c} ${d}`);
}

function CurryN(numberOfArg, func) {
    let numberRecursion = numberOfArg;
    let funcForRecursion = func;

    return function forRecursionFunc(x) {

        if (numberRecursion === 1) {
            return funcForRecursion = funcForRecursion(x);
        } else {
            --numberRecursion;
            funcForRecursion = funcForRecursion.bind(null, x);

            return forRecursionFunc;
        }
    }
}

// let funcForCurry = CurryN(4, printLetters);
//
// funcForCurry('A')('B')('C')('D');

//---------------------------------exercise_3------------------------------------------------

let obj = {
    key10: 'value10',
    obj10: {
        key20: true,
        obj20: {
            key30: 'value30',
            obj30: {
                key40: function plusOne(x) {
                    return x + 1;
                }
            }
        },
        key21: 'value21'
    },
    key11: 'value11',
    obj11: {
        key20: 20
    },
    key12: 'value12'
};

function deepCopyObjFunc(obj) {
    let copiedObj = {};

    for (let key in obj) {
        copiedObj[key] = obj[key];

        if (typeof copiedObj[key] === "object") {
            copiedObj[key] = deepCopyObjFunc(copiedObj[key]);
        }
    }

    return copiedObj;
}

let copiedObj = deepCopyObjFunc(obj);

// console.log(copiedObj);
// console.log('copiedObj function plusOne(2) in key40 is ---> ' + copiedObj.obj10.obj20.obj30.key40(2));

//---------------------------------exercise_4------------------------------------------------

function timeEveryMinuteFunc() {
    setInterval(function() {
        console.log(Date().slice(16,-24));
    },60000);

    return Date().slice(16,-24);
}

// console.log(timeEveryMinuteFunc());