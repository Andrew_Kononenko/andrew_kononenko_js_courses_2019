function log() {
    console.log(arguments);
}

function debounce(func, mlSeconds) {
    let createTimer;

    return function(args) {
        clearTimeout(createTimer);
        createTimer = setTimeout(func, mlSeconds, args);
    }
}


let debouncedFirst = debounce(log, 500);
let debouncedSecond = debounce(log, 500);
let debouncedThird = debounce(log, 500);

debouncedFirst('1');
debouncedFirst('2');
// --- 500 ms ---
// '2'
debouncedSecond('3');
// --- 500 ms ---
// '3'
debouncedThird('4');
debouncedThird('5');
debouncedThird('6');
// --- 500 ms ---
// '6'